#Version 2 de la calculadora

#Metodos de operaciones

def suma (num1, num2):
    return num1 + num2
def resta (num1, num2):
    return num1 - num2
def multiplicacion (num1, num2):
    return num1 * num2
def division (num1, num2):
    return num1 / num2
def potencia (num1, num2):
    return num1 ** num2

#Metodo default
def default():
    return "Opcion no valida"

#Metodo switch
def switch (opcion, num1, num2): #parámetro opcion
    #definicion de diccionario --> sinaxis con llaves
    diccionario = {1:suma(num1, num2), 2:resta(num1, num2), 3: multiplicacion(num1, num2), 4:division(num1, num2), 5:potencia(num1, num2)}
    return diccionario.get(opcion, default())
#Funcion del diccionario: simular un switch, ya que un switch no es permitido en python

print("---Calculadora----")
print("1) Suma")
print("2) Resta")
print("3) Multiplicacion")
print("4) Division")
print("5) Potencia")
opc = int(input("Ingresa una opcion"))

if(opc > 0 and opc <= 5):
    num1 = int(input("Ingresa un primer numero: "))
    num2 = int(input("Ingresa un segundo numero: "))
    print("El resultado es: ",switch(opc, num1, num2))